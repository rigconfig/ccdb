# CCDB - Computer Component Database

An open source MIT licensed repository for all computer component specifications.

# About

CCDB seeks to catalog and make freely available all known computer component specifications. This includes component properties, features, compatibility and also standardized 3D object definitions for representing components in 3D space using [THREE.js](https://threejs.org) or other 3D graphics rendering libraries. The idea came about while developing [RigConfig](https://gitlab.com/rigconfig/rigconfig). Why not? How hard could it be? *(famous last words)*

# Contribute

## Add / Edit Components

- Fork the repository and add/edit files in `src/json/**/*.json`
- Then send a merge request in a new branch to submit your changes.
- Conventions:
  - File name is the `part_number` _(e.g. BX80662I76700K.json)_
  - Extension is `.json`
  - Directory is the `type` property _(e.g. "cpus")_
    - **Example:** `src/json/cpus/BX80662I76700K.json`
  - See [existing json files](https://gitlab.com/rigconfig/ccdb/tree/master/src/json) for an example. Formal schemas will be added later.
  - You can also use the mongo db to add components located at `dist/mongo/ccdb`. Then run `utils/export_json.js` to export your newly added components to their `src/json/**/*.json` file equivalents.
  - Any questions, please post in the issue tracker. I would be happy to help.

## Bulk Submission

- If you have a lot of components to add to the database in bulk, please either post in the issue tracker or email [ccdb@rigconfig.com](mailto:ccdb@rigconfig.com).
- All formats are accepted. CSV _(comma delimited file(s))_, spreadsheets, database dumps _(mongo, mysql, etc)_. I will format the fields and values. Most of the schemas aren't ready yet, since only cpus have been added.
  - Component schemas will be in `src/schemas` _**(@todo: Add schemas)**_
- If you would like to use the repository and send a merge request, please add your files to the `incoming` directory of the repo under a descriptive directory name like `incoming/nvidia-video-cards/`.


# Usage

- Currently, the single source of truth is the directory of flat json files located at: `src/json/**/*.json`.
- These files are read by `utils/import_json.js` and inserted into the main mongo db `dist/mongo/ccdb`
  - _Note: You will need to have the mongo db running before executing_ `import_json.js`
  - e.g. `mongod --dbpath dist/mongo/ccdb`
- `utils/export_json.js` does the reverse. It exports the contents of the mongo db to flat json files in `src/json/{type}/{product_number}.json`
- This is a work in progress and will change in the future.

# Component Categories

- CPUs
- Motherboards
- Memory
- Video
  - Video Cards
  - Monitors
  - Adapters
  - Cooling
  - Capture
  - Accessories
- Cases
  - Computer Cases
  - Case Fans
  - External Enclosures
  - Case lighting
  - Controller Panels
- Power
  - Power Supplies
  - Power Cables
    - Internal
    - External
  - UPS
  - AC/DC Adapters
- Cooling
  - CPU Fans
  - Thermal Paste
  - Case Fans
  - Fan Controllers
  - Liquid Cooling
  - Case Accessories
- Storage
  - HDD
    - Internal
    - External
  - SSD
    - Internal
    - External
  - Optical Drives
- Sound
  - Sound Cards
  - Speakers
  - Headphones
- Peripherals
  - Keyboards
  - Mice
  - Mouse Pads
  - Trackpads
  - Wrist rests
- Cables
  - Internal Power Cables
  - External Power Cables
  - IDE Cables
  - SATA Cables
  - AC/DC Adapters
- Networking
  - Wireless
  - Wired

**Notes on categorization:**
- Root category is currently `type`
- Sub categories will be added via _algolia > style_ properties, which allow for multiple category assignment.

```javascript
{
  "objectID": "123",
  "name": "orange",
  "categories": {
    "lvl0": ["fruits", "color"],
    "lvl1": ["fruits > citrus", "color > orange"]
  }
},
{
  "objectID": "456",
  "name": "grapefruit",
  "categories": {
    "lvl0": ["fruits", "color", "new"],
    "lvl1": ["fruits > citrus", "color > yellow", "new > citrus"]
  }
}
```
> See: https://community.algolia.com/algoliasearch-helper-js/reference.html#hierarchical-facets

# Legal

All brands, logos, service and product names are property of their respective owners. All company, product and service names present in this repository are for identification purposes only. Use of these names, logos, and brands does not imply endorsement.

If you are a copyright, trademark or other intellectual property rights holder and would like to request removal of your component specs or alter the way your brand or trademark is stylized or attributed please contact [ccdb@rigconfig.com](mailto:ccdb@rigconfig.com).

Since this an open source project, you are also welcome to fork the project, alter the database as you see fit and submit your changes with a [merge request](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html). For public questions or requests, please post in the [issue tracker](https://gitlab.com/rigconfig/ccdb/issues). Thank you.
