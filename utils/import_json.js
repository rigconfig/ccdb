var sprintf         = require('sprintf-js').sprintf;
var assert          = require('assert');
var rread           = require('readdir-recursive');
var fs              = require('fs');
var MongoClient     = require('mongodb').MongoClient;
var mongo_host      = 'localhost';
var mongo_port      = '27017';
var mongo_db        = 'ccdb';
// var mongo_port      = '3001';
// var mongo_db        = 'meteor';
var credentials     = '';
// var credentials     = 'username:password';
var mongo_url       = sprintf('mongodb://' + (credentials && credentials + '@') + '%s:%s/%s', mongo_host, mongo_port, mongo_db);
var base_dir        = "../src/json";
var json_files      = [];

/** Summary:
      This file reads json files into a running mongo db target.
    Usage:
      - Update target database cridentials near top of file (host, port, db name)
      - Run command:
      > node import_json
*/

files = rread.fileSync(base_dir);

files.map((f, i) => {
  if (f.indexOf(".json") > 0) {
    var file_data = fs.readFileSync(f, 'utf8');

    if (file_data && isJSONParseable(file_data)) {
      json_files.push(JSON.parse(file_data));
    } else {
      console.log('*** WARN *** file_Data not json parseable!');
    }
  }
});

MongoClient.connect(mongo_url, function(err, db) {
  assert.equal(null, err);
  var count = 0;

  json_files.map((doc, i) => {
    // console.log(i, doc._id, doc.type, doc.part_number);
    if (doc.type) {
      var collection_name = doc.type;
      var collection = db.collection(collection_name);
      console.log('collection_name:', collection_name);
      // var res = collection.update({_id: doc._id}, doc, {upsert: true});
      // console.log('res:', res);
      collection.update({_id: doc._id}, doc, {upsert: true});
      count++;
    } else {
      console.log('*** WARN *** No "type" field present. Not inserted. (_id: %s) < ***', doc._id);
    }

  });

  db.close();
  console.log('***> Import complete!\n%s records added to mongo db: %s', count, mongo_db);
});

// SO: http://stackoverflow.com/a/3710506/2480125
function isJSONParseable(text) {
  if (/^[\],:{}\s]*$/.test(text.replace(/\\["\\\/bfnrtu]/g, '@').
    replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
    replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
      // the json is ok
      return true;
    } else {
      // the json is not ok
      return false;
    }
}
