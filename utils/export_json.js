var sprintf         = require('sprintf-js').sprintf;
var assert          = require('assert');
var fs              = require('fs');
var mkdirp          = require('mkdirp');
var MongoClient     = require('mongodb').MongoClient;
var mongo_host      = 'localhost';
var mongo_port      = '27017';
var mongo_db        = 'ccdb';
var mongo_url       = sprintf('mongodb://%s:%s/%s', mongo_host, mongo_port, mongo_db);
var base_dir        = "../src/json";

/** Summary:
      This file creates a directory of json files from the main ccdb mongo db.
    Usage:
      > node export_json
*/

MongoClient.connect(mongo_url, function(err, db) {
  assert.equal(null, err);

  db.listCollections().toArray(function(err, collections) {
    collections.map((collection) => {
      // console.log('collection:', collection);
      export_json_files(db, collection.name);
    });

    // db.close();
    setTimeout(() => {console.log('Export complete!  [Crtl + C] to exit');}, 6000); // fkit..
  });
});

// ______________________________________________________________________

function findDocuments(db, collection_name, callback) {
  var collection = db.collection(collection_name);
  collection.find({}, {}).toArray(function(err, docs) {
    assert.equal(err, null);
    callback(docs);
  });
}

function export_json_files(db, collection) {
  findDocuments(db, collection, function(docs) {
    docs.forEach(function (c,i) {
      (!c.type) && console.log("****************** > NO type! < ******************");
      (!c.part_number) && console.log("****************** > NO part_number! < ******************");

      var target_dir  = [base_dir, c.type.toLowerCase()].join("/");

      (mkdirp.sync(target_dir)) && console.log("CREATED DIRECTORY:", target_dir);

      var filename = [target_dir, c.part_number].join("/") + ".json";

      // Write files to approprate directory
      // @todo - Take into account multiple dirs levels / subcats. Just using one level atm.
      fs.writeFile(filename, JSON.stringify(c, null, 2), function(err) {
        if (err) {
          return console.log(err);
        }
        console.log("CREATED FILE: %s", filename);
      });
    });
  });
}
