var sprintf         = require('sprintf-js').sprintf;
var assert          = require('assert');
var rread           = require('readdir-recursive');
var fs              = require('fs');
var MongoClient     = require('mongodb').MongoClient;
var mongo_host      = 'localhost';
var mongo_port      = '27017';
var mongo_db        = 'ccdb';
// var mongo_port      = '3001';
// var mongo_db        = 'meteor';
var credentials     = '';
// var credentials     = 'username:password';
var mongo_url       = sprintf('mongodb://' + (credentials && credentials + '@') + '%s:%s/%s', mongo_host, mongo_port, mongo_db);
var base_dir        = "../src/json";
var json_files      = [];

files = rread.fileSync(base_dir);
// console.log(files);

files.map((f, i) => {
  if (f.indexOf(".json") > 0) {
    var file_data = fs.readFileSync(f, 'utf8');

    if (file_data && isJSONParseable(file_data)) {
      json_files.push(JSON.parse(file_data));
    } else {
      console.log('=> No file data or not parseable by isJSONParseable:', f);
    }
  }
});
// console.log('json_files.length', json_files.length);

json_files.map((f, i) => {
  console.log(f.type, f.part_number);
});










// SO: http://stackoverflow.com/a/3710506/2480125
function isJSONParseable(text) {
  if (/^[\],:{}\s]*$/.test(text.replace(/\\["\\\/bfnrtu]/g, '@').
    replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
    replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
      // the json is ok
      return true;
    } else {
      // the json is not ok
      return false;
    }
}
