# Research notes for acquiring component data

- [CPUs / Processors](#cpus-processors)
- [Motherboards](#motherboards)
- [Video Cards](#video-cards)
- [Memory](#memory)
- [Storage](#storage)
  - [HDD](#hdd)
    - [Internal](#internal-hdd)
    - [External](#external-hdd)
  - [SSD](#ssd)
- [Cases](#cases)
- [Power Supplies](#power-supplies)
- [Sound Cards](#sound-cards)

## CPUs / Processors

- Intel
- AMD

---

## Motherboards

### MFTRs
- ABIT
- AOpen
- ASRock
- ASUS
- Biostar
- Chaintech
- DFI
- ECS
- Epox
- EVGA
- FIC
- Foxconn
- Gigabyte
- Intel
- Jetway
- MSI
- PC CHIPS
- Sapphire
- Shuttle
- Soltek
- Soyo
- Tyan
- Wibtek
- XFX
- ZOTAC
- Acer
- Compaq
- Dell
- eMachines
- HP

---

## Video Cards

### MFTRs

- Asus
- Biostar
- Chaintech
- Club 3D
- Diamond Multimedia
- ECS
- ELSA Technology
- EVGA Corporation
- Foxconn
- Gainward
- Galax
- Gigabyte Technology
- HIS
- Hercules
- inno
- Leadtek
- Matrox
- Nvidia
- MSI
- PNY
- Point of View
- PowerColor
- S3 Graphics
- Sapphire Technology
- SPARKLE
- XFX
- Palit
- Zotac
- AMD
- BFG (defunct)
- EPoX (defunct)
- Oak Technology (defunct)
- 3dfx Interactive (defunct)

---

## Memory

- ADATA
- Apacer
- Asus
- Axiom
- Buffalo Technology
- Chaintech
- Corsair Memory
- Crucial
- Dataram
- Fujitsu
- G.Skill
- GeIL
- HP
- IBM
- Infineon
- Kingston Technology
- Lenovo
- Micron Technology
- Mushkin
- Nanya
- PNY
- Rambus
- Ramtron International
- Rendition
- Renesas Technology
- Samsung Semiconductor
- Sandisk
- SK Hynix
- Sony
- Strontium Technology
- Super Talent
- Toshiba
- Transcend
- Wilk Elektronik
- Winbond
- Wintec

---

## Storage

### HDD

#### Internal HDD

Seagate
Maxtor
Samsung
Toshiba
Western Digital
HGST

#### External HDD

ADATA
Buffalo Technology
Freecom
G-Technology
HGST
Hyundai
IoSafe
LaCie
LG
Promise Technology
Samsung
Seagate
Silicon Power
Sony
Toshiba
Transcend
TrekStor
Verbatim
Western Digital

### SSD

ADATA
Angelbird Technologies
Apacer
ASUS "Rog Raidr"
ATP Electronics
Corsair Memory
Crucial
Curtiss-Wright
Fusion-io Now SanDisk
Fujitsu
Greenliant Systems
G.Skill
Hitachi Global Storage Technologies
IBM
Indilinx
Intel
Kingston Technology
Lexar
LSI Corporation Flash division bought by Seagate
Memoright
Micron Technology
Mushkin
OCZ Technology
PNY Technologies
Ritek/RiDATA
RunCore
Samsung Electronics
SandForce
SanDisk
Seagate
SK Hynix
STEC, Inc.
Strontium Technology
SuperTalent
Texas Memory Systems
Toshiba
Transcend
Verbatim
Viking Modular Solutions
Violin Memory
Zalman
Zotac "SONIX"
Goodram

---

## Cases

- AMAX Information Technologies
- Antec
- AOpen
- APEVIA
- ASRock
- Auzentech
- Be quiet!
- Chassis Plans
- Cooler Master
- Corsair Components
- Dell
- Deepcool
- DFI
- ECS
- EVGA Corporation
- Foxconn
- Fractal Design
- Gigabyte Technology
- HP
- IBall
- Intel
- In Win Development
- Lenovo
- Lian Li
- MSI
- MiTAC
- NZXT
- RAIDMax
- Razer
- Rosewill
- Shuttle
- SilverStone Technology
- Supermicro
- Thermaltake
- Trenton Technology
- Ultra Products
- XFX
- Zalman

---

## Power Supplies

- Adata
- Akasa
- Antec
- APEVIA
- Arctic
- Be quiet!
- BFG Technologies
- Cooler Master
- Corsair
- Deepcool
- Dynex
- EVGA Corporation
- Fractal Design
- Foxconn
- FSP Group
- Gigabyte Technology
- In Win Development
- Lian-Li
- LiteOn
- Maplin
- OCZ Technology
- PC Power and Cooling
- RAIDMax
- Seasonic
- Seventeam
- SilverStone
- StarTech.com
- Thermaltake
- Trust
- XFX
- Xilence
- Zalman

---

## Sound Cards

- Ad Lib, Inc.
- Gravis
- Analog Devices
- Asus
- Aureal Semiconductor
- Auzentech
- C-Media
- Conrad
- Creative Technology
- Diamond Multimedia
- Avid Audio
- E-MU Systems
- Ensoniq
- Focusrite
- Hercules
- HT Omega
- Korg
- Lexicon
- M-Audio
- MOTU
- PreSonus
- Razer
- Realtek
- RME
- Sabrent
- Speedlink
- StarTech
- TerraTec
- Turtle Beach
- VIA


---

## Resources

- https://en.wikipedia.org/wiki/List_of_computer_hardware_manufacturers
